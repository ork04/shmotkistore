<?php

include_once 'CatalogFileConstants.php';

class XmlParser
{

    /**
     * @var string
     */
    protected $xmlFile;

    /**
     * @var \XMLReader
     */
    protected $reader;

    /**
     * @var DOMDocument
     */
    protected $doc;

    /**
     * @var array
     */
    protected $categoryList = [];

    /**
     * @var array
     */
    protected $productList = [];

    /**
     * @var false|mysqli
     */
    protected $con;

    public function __construct(string $xmlFile, array $config)
    {
        $this->xmlFile = $xmlFile;
        $this->reader = new XMLReader();
        $this->doc = new \DOMDocument;
        $host = ($config['host']) ?? $config['host'];
        $dbName = ($config['dbName']) ?? $config['dbName'];
        $user = ($config['user']) ?? $config['user'];
        $password = ($config['password']) ?? $config['password'];
        $this->con= mysqli_connect($host, $user, $password, $dbName);
    }

    /**
     * Передаем ридеру, что будем парсить этот файл
     */
    public function openFile(): void
    {
        $this->reader->open($this->xmlFile);
    }

    /**
     * @return array
     */
    public function readCategories(): void
    {
	mysqli_query($this->con, 'SET NAMES utf8');

        while ($this->reader->read()) {
            if ($this->reader->nodeType == XMLReader::ELEMENT) {
                if ($this->reader->localName == CatalogFileConstants::CATEGORIES) {
                    while ($this->reader->read()) {
                        if ($this->reader->nodeType == XMLReader::ELEMENT && $this->reader->name == CatalogFileConstants::CATEGORY) {
                            $categoryId = $this->reader->getAttribute('id');
                            $this->categoryList[$categoryId]['parent_id'] = $this->reader->getAttribute('parent_id');
                            $this->reader->read();
                            if ($this->reader->nodeType == XMLReader::CDATA) {
                                $this->categoryList[$categoryId]['description'] = $this->reader->value;
                            }
                        }
                    }
                }
            }
        }
        foreach ($this->categoryList as $key => $category) {
            $this->writeCategories($key,$category);
        }
    }

    public function readProducts()
    {
        try {
            mysqli_query($this->con, 'SET NAMES utf8');
            $this->backupTables();
            $valuesCnt = 0;
            $valuesProductsList = array();
            $valueProductsColorAttributeList = array();
            $valueProductsSexAttributesList = array();
            $valueProductsSizeAttributesList = array();
            $crookedColors = $this->castColorAttribute();

            while ($this->reader->read()) {

                if ($this->reader->nodeType == XMLReader::ELEMENT && $this->reader->name == CatalogFileConstants::OFFERS) {
                    while ($this->reader->read()) {

                        if ($this->reader->nodeType == XMLReader::ELEMENT && $this->reader->name == CatalogFileConstants::OFFER) {
                            $dom = simplexml_import_dom($this->doc->importNode($this->reader->expand(), true));
                            $productId = htmlentities((string)$dom->attributes()->id);

                            $available = ((string)$dom->attributes()->available == 'true') ? 1 : 0;
                            $merchantId = (string)$dom->attributes()->merchant_id;
                            $gsProductKey = (string)$dom->attributes()->gs_product_key;
                            $article = $this->clearValue((string)$dom->attributes()->article);
                            $gsCategoryId = (string)$dom->attributes()->gs_category_id;
                            $picture = str_replace(' ','', (string)$dom->picture[0]);
                            $thumbnail = (string)$dom->thumbnail[0];
                            $originalPicture = (string)$dom->original_picture[0];
                            $otherPictures = $this->getPictures($dom);
                            $name =  stripslashes(str_replace("'","\"",(string)$dom->name[0]));
                            $description = stripslashes(htmlentities((string)$dom->description[0], ENT_QUOTES));
                            $vendor = stripslashes(htmlentities(str_replace("'","",(string)$dom->vendor[0])));
                            $model = stripslashes(htmlentities(str_replace("'","",(string)$dom->model[0])));
                            $oldprice = (string)$dom->oldprice[0];
                            $url = str_replace("'","\"",(string)$dom->url[0]);
                            $destinationUrl = (string)$dom->{'destination-url-do-not-send-traffic'}[0];
                            $currencyId = (string)$dom->currencyId[0];
                            $price = $this->clearValue((string)$dom->price[0]);

                            $size = null;
                            $age = null;
                            $sex = null;
                            $composition = null;
                            $color = null;

                            foreach ($dom->param as $param) {
                                $paramName = $param->attributes()->name;
                                switch ($paramName) {
                                    case 'Размер':
                                        $size = $this->filterSize($param);
                                        break;
                                    case 'Возраст':
                                        $age = $param;
                                        break;
                                    case 'Пол':
                                        $sex = $this->filterSex($param);
                                        break;
                                    case 'Состав':
                                        $composition = $param;
                                        break;
                                    case 'Цвет':
                                        $color = $this->clearValue($param); 
                                        $color = str_replace('/',' ',$color);
                                        $color = $this->fixColor($crookedColors,$color);
                                        $color = $this->colorParser($color);
                                        break;
                                }
                            }

                            $oldprice = ($oldprice == '') ? '0.00' : $oldprice;

                            $valuesCnt++;
                            $valuesProductsList[] = "('$productId','$article','$name','$description','$available',
                                $merchantId,'$gsProductKey',$gsCategoryId,'$picture','$thumbnail','$originalPicture',
                                '$vendor','$model','$oldprice','$url','$destinationUrl','$currencyId','$price','$age','$composition', '$otherPictures')";

                            if (!empty($sex)) {
                                $valueProductsSexAttributesList[] = "($productId,'$sex')";
                            }

                            if (!is_null($color)) {
                                $valueProductsColorAttributeList[] = $this->prepareColorAttributeToSave($productId, $color);
                            }
                            
                            if (!empty($size)) {
                                $valueProductsSizeAttributesList[] = $this->prepareSizeAttributeToSave($productId, $size);
                            }
                            
                            
                        }

                        if ($valuesCnt == 100) {
                            $sqlInsertInProducts = "INSERT INTO products (product_id,article,name,description,available,merchant_id,gs_product_key
                                ,gs_category_id,picture,thumbnail,original_picture,vendor,model,oldprice,url,destination_url,currencyId,
                                price,age,composition, other_pictures) VALUES" . implode(',', $valuesProductsList);
                            mysqli_query($this->con,$sqlInsertInProducts) or die(mysqli_error($this->con) . $sqlInsertInProducts);
                            
                            if (sizeof($valueProductsSexAttributesList) >= 100) {
                                $sqlInsertInSexAttributes = "INSERT INTO sex (product_id, sex) VALUES" . implode(',', $valueProductsSexAttributesList);
                                mysqli_query($this->con, $sqlInsertInSexAttributes) or die(mysqli_error($this->con) . $sqlInsertInSexAttributes);
                            }

                            if (sizeof($valueProductsColorAttributeList) >= 100) {
                                $sqlInsertInColors = "INSERT INTO colors (product_id, color) VALUES" . implode(',', $valueProductsColorAttributeList);
                                mysqli_query($this->con, $sqlInsertInColors) or die(mysqli_errno($this->con) . mysqli_error($this->con). $sqlInsertInColors);
                            }
                            
                            if (sizeof($valueProductsSizeAttributesList) >= 100) {
                                $sqlInsertInSize = "INSERT INTO size (product_id, size) VALUES" . implode(',', $valueProductsSizeAttributesList);
                                mysqli_query($this->con, $sqlInsertInSize) or die(mysqli_error($this->con) . $sqlInsertInSize);
                            }

                            $valuesCnt = 0;
                            $valuesProductsList = array();
                            $valueProductsColorAttributeList = array();
                            $valueProductsSexAttributesList = array();
                            $valueProductsSizeAttributesList = array();
                        }

                    }
                }

            }

            return $this->productList;
        } catch (Exception $err) {
            print $err->getMessage();
            $this->recoveryBackupTables();
            unlink($this->xmlFile);
        }
    }

    private function backupTables()
    {
        $sqlExistTable = "SHOW TABLES LIKE 'products_bck'";
        $mQuery = mysqli_query($this->con, $sqlExistTable) or die(mysqli_error($this->con) . $sqlExistTable);
        $row = mysqli_fetch_row($mQuery);

        if (!empty($row)) {
            $sqlDropTableProductsBck = "DROP TABLE products_bck";
            @mysqli_query($this->con, $sqlDropTableProductsBck) or die(mysqli_error($this->con));
            $sqlDropTableColorsBck = "DROP TABLE colors_bck";
            @mysqli_query($this->con, $sqlDropTableColorsBck) or die(mysqli_error($this->con));
            $sqlDropTableSexBck = "DROP TABLE sex_bck";
            @mysqli_query($this->con, $sqlDropTableSexBck) or die(mysqli_error($this->con));
            $sqlDropTableSizeBck = "DROP TABLE size_bck";
            @mysqli_query($this->con, $sqlDropTableSizeBck) or die(mysqli_error($this->con));
            $sqlDropTableCategoryProductCntBck = "DROP TABLE category_product_cnt_bck";
            @mysqli_query($this->con, $sqlDropTableCategoryProductCntBck) or die(mysqli_error($this->con));
        }

        $this->createTablesBackup();

        $sqlTruncateProducts = "TRUNCATE TABLE products";
        mysqli_query($this->con, $sqlTruncateProducts) or die(mysqli_error($this->con));
        $sqlTruncateColors = "TRUNCATE TABLE colors";
        mysqli_query($this->con, $sqlTruncateColors) or die(mysqli_error($this->con));
        $sqlTruncateSex = "TRUNCATE TABLE sex";
        mysqli_query($this->con, $sqlTruncateSex) or die(mysqli_error($this->con));
        $sqlTruncateSize = "TRUNCATE TABLE size";
        mysqli_query($this->con, $sqlTruncateSize) or die(mysqli_error($this->con));
        $sqlTruncateCategoryProductCnt = "TRUNCATE TABLE category_product_cnt";
        mysqli_query($this->con, $sqlTruncateCategoryProductCnt) or die(mysqli_error($this->con));
    }
    
    private function recoveryBackupTables() {
        $sqlDropProducts = "DROP TABLE 'products'";
        mysqli_query($this->con, $sqlDropProducts) or die(mysqli_error($this->con));
        $sqlDropSex = "DROP TABLE 'sex'";
        mysqli_query($this->con, $sqlDropSex) or die(mysqli_error($this->con));
        $sqlDropSize = "DROP TABLE 'size'";
        mysqli_query($this->con, $sqlDropSize) or die(mysqli_error($this->con));
        $sqlDropCategoryProductCnt = "DROP TABLE 'category_product_cnt'";
        mysqli_query($this->con, $sqlDropCategoryProductCnt) or die(mysqli_error($this->con));
        
        $sqlRecoveryProducts = "RENAME TABLE products_bck TO products";
        mysqli_query($this->con, $sqlRecoveryProducts) or die(mysqli_error($this->con));
        $sqlRecoverySex = "RENAME TABLE sex_bck TO sex";
        mysqli_query($this->con, $sqlRecoverySex) or die(mysqli_error($this->con));
        $sqlRecoverySize = "RENAME TABLE size_bck TO size";
        mysqli_query($this->con, $sqlRecoverySize) or die(mysqli_error($this->con));
        $sqlRecoveryCategoryProductCnt = "RENAME TABLE category_product_cnt_bck TO category_product_cnt";
        mysqli_query($this->con, $sqlRecoveryCategoryProductCnt) or die(mysqli_error($this->con));
    }

    protected function createTablesBackup()
    {
        $sqlCreateAndCopyTable = "CREATE TABLE products_bck SELECT * FROM products";
        @mysqli_query($this->con, $sqlCreateAndCopyTable) or die(mysqli_error($this->con) . $sqlCreateAndCopyTable);
        
        $sqlCreateAndCopyTableColors = "CREATE TABLE colors_bck SELECT * FROM colors";
        @mysqli_query($this->con, $sqlCreateAndCopyTableColors) or die(mysqli_error($this->con) . $sqlCreateAndCopyTableColors);
        
        $sqlCreateAndCopyTableSex = "CREATE TABLE sex_bck SELECT * FROM sex";
        @mysqli_query($this->con, $sqlCreateAndCopyTableSex) or die(mysqli_error($this->con) . $sqlCreateAndCopyTableSex);
        
	$sqlCreateAndCopyTableSize = "CREATE TABLE size_bck SELECT * FROM size";
        @mysqli_query($this->con, $sqlCreateAndCopyTableSize) or die(mysqli_error($this->con) . $sqlCreateAndCopyTableSize);

        $sqlCreateAndCopyTableCategoryProductCnt = "CREATE TABLE category_product_cnt_bck SELECT * FROM category_product_cnt";
        @mysqli_query($this->con, $sqlCreateAndCopyTableCategoryProductCnt) or die(mysqli_error($this->con) . $sqlCreateAndCopyTableCategoryProductCnt);

    }

    protected function writeCategories(int $id, array $category): void
    {
        $parent_id = ($category['parent_id']) ?? 0;
        $description = ($category['description']) ?? 0;

        $sql = "SELECT * FROM categories WHERE id=$id";
        $m = mysqli_query($this->con, $sql)  or die(mysqli_error($this->con));
        $row = mysqli_fetch_assoc($m);

        if (is_null($row)) {
            $sqlInsert = "INSERT INTO categories SET id=$id, parent_id=$parent_id, description='$description'";
            mysqli_query($this->con, $sqlInsert) or die(mysqli_error($this->con));
        } else {
            if ($row['parent_id'] != $parent_id || $row['description'] != $description) {
                $sqlUpdate = "UPDATE categories SET parent_id=$parent_id, description='".$description."' WHERE id=$id LIMIT !";
                mysqli_query($this->con, $sqlUpdate)  or die(mysqli_error($this->con));
            }
        }

    }

    protected function clearValue($str)
    {
        $str = stripslashes($str);
        $str = str_replace("'","\"",$str);
        $str = htmlentities($str, ENT_QUOTES);

        return $str;
    }

    public function deleteXmlFile()
    {
	    unlink('/var/www/html/parser/' . $this->xmlFile);
    }

    public function setStatus($status)
    {
	    $sql = "UPDATE status SET status = '$status'";
	    mysqli_query($this->con, $sql);
    }

    public function castColorAttribute()
    {
        $correctedColors = [];
        $sql = "SELECT * FROM color_check_keys";
        $correctedColorSqlRows = mysqli_query($this->con, $sql) or die (mysqli_error($this->con));
        $i = 0;
        while ($row = mysqli_fetch_assoc($correctedColorSqlRows)) {
            $wrongColorsList = explode(',', $row['wrong_color']);
            $correctedColors[$i]['wrong'] = (is_array($wrongColorsList)) ? $wrongColorsList : $row['wrong_color'];
            $correctedColors[$i]['color'] = $row['right_color'];
            $i++;
        }

        return $correctedColors;
    }

    public function fixColor($correctedColors, $wrongColor)
    {
       foreach ($correctedColors as $key => $color) {
            if (in_array($wrongColor, $color['wrong'])) {
                return $color['color'];
            }
        }
        
       return $wrongColor;
    }

    public function prepareColorAttributeToSave($productId, $productColorList)
    {
        $sql = "";
        $sqlValues = [];

        if (is_array($productColorList)) {
            foreach ($productColorList as $key => $color) {
                $sqlValues[] = "(".$productId.", '".$color."')";
            }
            $sql = implode(',', $sqlValues);
        }

        if (empty($sql)) {
            $sql = "(".$productId.", '".$productColorList."')";
        }
        
        return $sql;
    }

    public function colorParser($wrongColor)
    {
        preg_match('/,| и |\s|and/', $wrongColor, $matches);
        
        if (!empty($matches)) {
            $separatorSymbol = $matches[0];
            $colorList = explode($separatorSymbol, $wrongColor);
            return $colorList;
        }

        return $wrongColor;
    }
    
    private function filterSex($sex)
    {
        preg_match('/муж|жен|дев|мал|подр|уни|все/i', $sex, $match);
       
        if (sizeof($match) == 0) {
            $result = 'недоступен';
            
            return $result;
        }
        
        switch ($match[0]) {
            case 'муж':
                $result = 'мужской';  
                break;
            case 'жен':
                $result = 'женский';
                break;
            case 'дев':
                $result = 'для девочек';
                break;
            case 'мал':
                $result = 'для мальчиков';
                break;
            case 'подр':
                $result = 'для подростков';
                break;
            case 'уни':
                $result = 'унисекс';
                break;
            case 'все':
                $result = 'для всех';
            default:
                $result = 'для всех';
                break;
        }
        
        return $result;   
    }
    
    private function filterSize($size)
    {
        $sizeList = [];
        preg_match('/,/', $size, $match);
        
        if (!empty($match)) {
            $sizeList = explode(',', $size);
            return $sizeList;
        }
        
        $sizeList[] = $size;
        
        return $sizeList;
    }
    
    private function prepareSizeAttributeToSave($productId, $sizeList)
    {
        $toInsertList = [];
        $sql = ''; 
        
        foreach ($sizeList as $key => $size) {
            $toInsertList[] = "(".$productId.",'".$size."')";
        }
        
        $sql = implode(',', $toInsertList);
        
        return $sql;
    }
    
    public function categoriesProductCount()
    {
        $sql = "SELECT gs_category_id AS 'category_id', COUNT(product_id) AS 'count' FROM products GROUP BY gs_category_id";
        $query = mysqli_query($this->con, $sql) or die(mysqli_error($this->con) . $sql);

        $sqlInsert = [];
        
        while ($row = mysqli_fetch_object($query)) {
            $sqlInsert[] = "($row->category_id, $row->count)"; 
        }
        
        $sqlInsert = "INSERT INTO category_product_cnt (category_id, product_cnt) VALUES" . implode(',', $sqlInsert);
        mysqli_query($this->con, $sqlInsert) or die(mysqli_error($this->con));
    }
    
    private function getPictures(&$dom) {
        $pictureList = [];

        foreach ($dom->picture as $picture) {
            $pictureList[] = (string)$picture;
        }

        return json_encode($pictureList);
    }

}

