<?php


class CatalogFileConstants
{
    const CATEGORIES = 'categories';

    const CATEGORY = 'category';

    const OFFERS = 'offers';

    const OFFER = 'offer';
}