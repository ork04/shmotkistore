<?php

ini_set('memory_limit', '768M');
set_time_limit(15000);
error_reporting(E_ALL);

include_once 'library/XmlParser.php';

$xmlFilesPath = '/root';

$dbConfig = [
    'host' => '31.31.196.80',
    'dbName' => 'u0305394_shmotki',
    'user' => 'u0305_shmotki',
    'password' => 'j3j7dM0~'
];

$remoteUrl = "http://exporter.gdeslon.ru/uploads/exports/21231679121f4fbc75f43dd9642ae59e1f9af748.xml.zip";
downloadFile($remoteUrl);
deleteXmlFiles($xmlFilesPath);

$file = exec("unzip  file.zip");
$file = str_replace('inflating:', '', $file);
$file = trim($file);

$xmlParser = new XmlParser($file,$dbConfig);
$xmlParser->setStatus(1);
$xmlParser->openFile();
$xmlParser->readCategories();
$xmlParser->openFile();
$xmlParser->readProducts();
$xmlParser->categoriesProductCount();
$xmlParser->deleteXmlFile();
$xmlParser->setStatus(0);

function downloadFile($remoteUrl)
{
    $ch = curl_init($remoteUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    $output = curl_exec($ch);
    $fh = fopen("file.zip", 'w');
    fwrite($fh, $output);
    fclose($fh);
}

function deleteXmlFiles($xmlFilesPath)
{
    $files = scandir($xmlFilesPath);
    foreach ($files as $key => $file) {
        if (substr($file,-3, 3) == 'xml') {
            unlink($xmlFilesPath.'/'.$file);
        }
    }
}